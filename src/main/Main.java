package main;

import main.model.NewsModel;

import java.util.Scanner;

public class Main {
    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        NewsGenerator.getInstance().addListener(new FirstListener());
        NewsGenerator.getInstance().addListener(new NewsGenerator.NewsListener() {
            @Override
            public void onNewsReceived(NewsModel newsModel) {
                System.out.println(this.getClass().getSimpleName() +
                        " " + newsModel.getText());
            }
        });
        while (!in.nextLine().equalsIgnoreCase("stop")) {

        }
        NewsGenerator.getInstance().stopSelf();
    }

    static class FirstListener implements NewsGenerator.NewsListener {
        @Override
        public void onNewsReceived(NewsModel newsModel) {
            System.out.println(this.getClass().getSimpleName() + " " +
                    newsModel.getText());
        }
    }
}
