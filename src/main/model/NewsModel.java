package main.model;

import java.util.Random;

public class NewsModel {
    private long date;
    private String text;

    private Random rnd = new Random();

    public NewsModel() {
        date = System.currentTimeMillis();
        text = "News number "+rnd.nextInt(100);
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
