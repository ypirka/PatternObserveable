package main;

import main.model.NewsModel;

import java.util.LinkedList;
import java.util.List;

public class NewsGenerator extends Thread {//observeable

    private static NewsGenerator instance;
    private List<NewsListener> newsListenerList = new LinkedList<>();
    private volatile boolean state = true;

    private NewsGenerator() {
        start();
    }

    public static NewsGenerator getInstance() {
        synchronized (NewsGenerator.class) {
            if (instance == null) {
                instance = new NewsGenerator();
            }
        }
        return instance;
    }

    public void addListener(NewsListener listener) {
        newsListenerList.add(listener);
    }

    public void removeListener(NewsListener listener) {
        newsListenerList.remove(listener);
    }

    @Override
    public void run() {
        for (; state; ) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            NewsModel newsModel = new NewsModel();
            for (NewsListener l : newsListenerList) {
                l.onNewsReceived(newsModel);
            }
        }
    }

    public void stopSelf() {
        state = false;
    }

    public interface NewsListener {
        void onNewsReceived(NewsModel newsModel);
    }
}
